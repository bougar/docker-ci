import unittest
import src.funcs as myFuncs

class TestStringMethods(unittest.TestCase):
    def test_func1(self):
        self.assertEqual(myFuncs.func1(), 'Func1')

    def test_func2(self):
        self.assertEqual(myFuncs.func2(), 5)

if __name__ == '__main__':
    unittest.main()
